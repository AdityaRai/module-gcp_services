#BUCKET 1

resource "google_storage_bucket" "bucket1" {
#Bucket name
 name = "jefin_test_bucket1"
count=1
labels = {
    key = "env" 
    value = "test"
}
location= "us-central1"			#location type=regiom
storage_class = "ARCHIVE"
uniform_bucket_level_access = false      #access contol =fine-grained
force_destroy = false
#object versioning
versioning  {
    enabled = true
  }
}

resource "google_storage_bucket_object" "text" {
  name   = "test_file"
  content = "Hi World"
  bucket = "${google_storage_bucket.bucket1[0].name}"
}
#bucket 2

resource "google_storage_bucket" "bucket4" {
#Bucket name
 name = "jefin_test_bucket4"
labels = {
    key = "env" 
    value = "test2"
}
location = "us" 		#location type=multiregion
storage_class = "STANDARD"
uniform_bucket_level_access = true

#retension policy:The period of time, in seconds, that objects in the bucket must be retained and cannot be deleted, overwritten, or archived. The value must be less than 2,147,483,647 seconds.

retention_policy {
  retention_period = 60 #IN SECONDS
  is_locked = true
}
}

resource "google_storage_bucket_object" "test-text2" {
  name   = "test_file"
  content = "Hi World"
  bucket = "${google_storage_bucket.bucket4.name}"
}

#bucket 3

resource "google_storage_bucket" "bucket3" {
#Bucket name
 name = "jefin_test_bucket3"
labels = {
    key = "env" 
    value = "test3"
}
location = "us" 		#location type=multiregion
storage_class = "COLDLINE"

uniform_bucket_level_access = true  #ENABLE THE UNIFORM LEVEL ACCESS CONTROL

#LIFE CYCLE RULE
#Each lifecycle management configuration contains a set of rules. When defining a rule, you can specify any set of conditions for any action. If you specify multiple conditions in a rule, an object has to match all of the conditions for the action to be taken

lifecycle_rule {
    condition {
      age = "1"
    }
    action {
     type = "Delete"
    }
  }
}



resource "google_storage_bucket_object" "test-text3" {
  name   = "test_file"
  content = "Hi World"
  bucket = "${google_storage_bucket.bucket3.name}"
}
