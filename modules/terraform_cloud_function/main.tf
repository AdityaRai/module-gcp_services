resource "google_storage_bucket" "bucket" {
#Number of bucket
 count = 1
#Bucket name
 name = "jefin_gcf_bucket01"
# Any location of your choice
 location = "europe-west2"
# Any storage_class of your choice
 storage_class = "STANDARD"
}

# Resource for store object
resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  # Storing object at index 0
  bucket = "${google_storage_bucket.bucket[0].name}"
  source = "./modules/terraform_cloud_function/index.zip"
}

resource "google_cloudfunctions_function" "function" {
  # A user-defined name of the function
  name        = "test-function-test"
  #The runtime in which the function is going to run
  runtime     = "nodejs16"

  available_memory_mb   = 128
  source_archive_bucket = "${google_storage_bucket.bucket[0].name}"
  source_archive_object = "${google_storage_bucket_object.archive.name}"
  trigger_http          = true
  # Name of the function that will be executed when the Google Cloud Function is triggered
  entry_point           = "helloWorld"
}
# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
