terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.27.0"
    }
  }
}

terraform {
  backend "gcs" {
    bucket  = "jefin_remote_backend" #BUCKET IS ALREADY CREATED IN GCP
    prefix  = "terraform/TFstateFILE"
    credentials = "keyfile.json"
  }
}
